import angular from 'angular';
import Home from './home/home';
import Todo from './todo/todo';


let componentModule = angular.module('app.components', [
  Home,
  Todo
])

.name;

export default componentModule;
