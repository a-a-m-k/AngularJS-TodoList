class TodoController {
	constructor($log,TodoService) {
    'ngInject';
    this.todoService = TodoService;
    this._console = $log;
       
  }
  $onInit() {
	  this.todos = [];
		this.todoService.getAll()
                .then((todos) => this.todos = todos);
		
  }
	addNewElement(todoLabel) {
            const todo = {id: this.todos.length + 1, text: todoLabel, done: false};
            this._console.log(todo);
            this.todoService.store(todo)
                .then((t) => {
                    this.todos.push(t);
                })
                .catch(alert);
        }

	removeElement({ index }) {
        const todo=this.todos[index];
         this._console.log(todo);
         
         this.todoService.delete(todo)
         .then((t) => {
                    this.todos.splice(index,1);
                })
                .catch(alert);
	
      
	}
  	

	archive() {
	  this.todos = this.todos.filter(todo =>
        !todo.done);
	}

    remaining() {
      const filtered = this.todos.filter(todo =>
        !todo.done);
    return filtered.length;
    }
   

        
}

export default TodoController;
