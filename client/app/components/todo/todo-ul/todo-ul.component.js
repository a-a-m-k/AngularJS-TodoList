import template from './todo-ul.html';
import controller from './todo-ul.controller';
import './todo-ul.scss';

let todoUlComponent = {
  bindings: {
    todos: '<',
    filter: '<',
     onRemoveEl: '&',

 remaining: '<'},
  template,
  controller
};

export default todoUlComponent;
