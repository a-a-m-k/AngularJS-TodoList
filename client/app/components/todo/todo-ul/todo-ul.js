import angular from 'angular';
import uiRouter from 'angular-ui-router';
import todoUlComponent from './todo-ul.component';

let todoUlModule = angular.module('todoUl', [
  uiRouter
])

.component('todoUl', todoUlComponent)
  
.name;

export default todoUlModule;
