import angular from 'angular';
import TodoService from './todo.service';
import uiRouter from 'angular-ui-router';
import todoComponent from './todo.component';
import TodoForm from './todo-form/todo-form';
import TodoUl from './todo-ul/todo-ul';




let todoModule = angular.module('todo', [
  uiRouter,
  TodoForm,
  TodoUl
  
])
.service('TodoService',TodoService)

.component('todo', todoComponent)

.name;

export default todoModule;
