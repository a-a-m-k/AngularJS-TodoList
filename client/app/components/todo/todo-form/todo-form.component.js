import template from './todo-form.html';
import controller from './todo-form.controller';


let todoFormComponent = {
  bindings: {
    todo: '<',
	   onNewElement: '&',
         onArchive: '&'
  },
  template,
  controller
};

export default todoFormComponent;
