import angular from 'angular';
import uiRouter from 'angular-ui-router';
import todoFormComponent from './todo-form.component';

let todoFormModule = angular.module('todoForm', [
  uiRouter
])

.component('todoForm', todoFormComponent)
  
.name;

export default todoFormModule;
