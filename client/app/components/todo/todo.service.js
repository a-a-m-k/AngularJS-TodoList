
const TASKS = 'tasks';

function getAllStored() {
    return JSON.parse(window.sessionStorage.getItem(TASKS)) || [];
}

function storeAll(todos) {
    window.sessionStorage.setItem(TASKS, JSON.stringify(todos));
}
function removeItem(todo) {
    window.sessionStorage.removeItem(todo);
}

class TodoService {
  /* @ngInject */
    constructor($log, $q) {
        this._console = $log;
        // used to simulate http promise
        this._q = $q;
        getAllStored.bind(this);
    }

    store(todo) {
        this._console.log('Storing' + JSON.stringify(todo));
        const todos = getAllStored();
        todos.push(todo);
        storeAll(todos);
        return this._q.resolve(todo);
    }
    delete(todo) {
        this._console.log('delete ' + todo.id);
        const todos = getAllStored();
        todos.splice(todo, 1);
        removeItem(todo);
        storeAll(todos);
        return this._q.resolve(todo);
    }

    getAll() {
        return this._q.resolve(getAllStored());
    }

  
}
export default TodoService;